##############################################
Customization Manager for Orchid Extender
##############################################

The Customization Manager for Orchid Extender unlocks the full potential
of Python for customizing Sage 300.  It enables quick and easy management
of the embedded Python installation used by Extender and supports installation
of most of the 195,000 packages from the `Python Packaging Index`_ or
any custom package from a version control system (VCS) repository.

For more information, check out the `documentation`_.

.. _documentation: https://customization-manager.readthedocs.io

.. _Python Packaging Index: https://pypi.org
