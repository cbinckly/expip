Uninstall a Customization
=========================

Uninstalling a customization is as easy as installing one:

1. Open the :guilabel:`Extender --> Setup --> Customization Manager` screen.
2. Highlight the customization and click :guilabel:`Uninstall`.
3. A message is displayed with the result.  
4. Close the Customization Manager window.

Done.

The Uninstall Button is disabled
--------------------------------------

The :guilabel:`Uninstall` button will be disabled if the customization
is not installed!

If you're having trouble uninstalling a customization, `contact us`_.

.. _contact us: mailto:chris@poplars.dev
