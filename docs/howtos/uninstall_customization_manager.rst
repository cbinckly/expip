Uninstall Customization Manager
===========================================

Customization Manager is distributed as an Extender module. Before
removing the application, consider 
:ref:`uninstalling<Uninstall a Customization>` all the customizations
managed by it first.

Uninstall the Module
--------------------

Uninstall the module from the Extender Modules screen.

   1. Open the :guilabel:`Extender --> Setup --> Modules` screen.
   2. Highlight the :guilabel:`Customization Manager` module.
   3. Press the ``Del`` (delete) key on your keyboard.

Customization Manager has been removed.

Remove the Icon from the Desktop
--------------------------------

If the Customization Manager icon has not been removed from the desktop,
you can remove it manually:

1. Open the :guilabel:`Extender --> Setup --> Script` screen.
2. Highlight the first script in the grid, right click, and select
   :guilabel:`Add to Desktop`.
3. A window with the desktop tree is displayed. Expand 
   :guilabel:`Extender --> Setup`.
4. Highlight the :guilabel:`Customization Manager` icon. Push the
   Delete (``Del``) key on your keyboard to remove the icon.
5. Do not use the :guilabel:`Save` or :guilabel:`Cancel` buttons in this step!
   Close the window using the close ``X`` in the title bar. 

.. warning::

    If you accidentally close the window using a button, fear not, no
    harm has been done.  However, you'll have to repeat the process
    from the beginning.

6. When prompted, save your changes to the desktop layout.

Restart the Sage desktop to complete the removal of the icon.
