Upgrade a Customization
=======================

Upgrading a customization with Customization Manager is easy:

1. Open the :guilabel:`Extender --> Setup --> Customization Manager` screen.
2. Highlight the customization row. The :guilabel:`Status` column will
   show ``Upgrade Available`` if a newer version can be installed.
3. Click the :guilabel:`Upgrade` button.
4. A message is displayed displaying the result.  
5. Close the Customization Manager window.

The customzation is now upgraded.  Restart the Sage desktop to allow
Extender to register new components.

The Upgrade Button is disabled
--------------------------------------

The :guilabel:`Upgrade` button will be disabled if:

- the customization is not yet installed, in which case its status will be
  ``Available``;
- the current Sage or Extender versions are not compatible with the 
  customization, the compatibility field will indicate which versions are
  required.

If you're having trouble installing modules for a customization, `contact us`_.

.. _contact us: mailto:chris@poplars.dev
