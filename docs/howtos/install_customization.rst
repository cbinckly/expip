Install a Customization
=======================

Installing a customization with Customization Manager is easy:

1. Open the :guilabel:`Extender --> Setup --> Customization Manager` screen.
2. A grid displays all the customizations that have been built for you. 
   Highlight the customization and click :guilabel:`Install`.
3. A message is displayed with the result.
4. Close the Customization Manager window.

All done. Restart the Sage Desktop to allow Extender to register the changes.

The Install Button is disabled
--------------------------------

The :guilabel:`Install` button will be disabled if:

- the customization is already installed, in which case its status will be
  either ``Up to date`` or ``Upgrade Available``;
- the current Sage or Extender versions are not compatible with the 
  customization, the compatibility field will indicate which versions are
  required.

If you're having trouble installing a customization, `contact us`_.

.. _contact us: mailto:chris@poplars.dev
