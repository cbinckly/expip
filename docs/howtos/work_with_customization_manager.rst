Work with Customization Manager
==================================

Customization Manager is intended to be a simple and easy to
use but it never hurts to have a few pointers when
getting started.

The interface is composed of the **Package Grid** and a set of
**Action Buttons**.

.. figure:: /static/images/customization_manager.png
   :alt: Customization manager screen.
   
   Customization Manager screen with the Package Grid
   above the Action Buttons.

The Pakage Grid
---------------

The package grid always displays all the customizations that
are associated with the configured API key, whether they are
installed or not.  

All customizations have a short *Name* that is used to 
uniquely identify them.  In the figure above, the
first customization is named ``expip`` and is Customization
Manager.

The name is followed by a meaningful *Description* of what the 
customization does.

There are two versions in the grid: the *Installed Version* and the
*Available Version*.  The *Installed Version* is the version running
in the current company; it is set to ``-`` for customizations that
are not installed.  The *Available Version* is the latest version
available for install.

All customizations have a *Status*.  The status can be any of:

- ``Available``: the customization is compatible and available 
  for installation.
- ``Up to Date``: the customization is installed and at the 
  latest version.
- ``Upgrade Available``: a newer version of the customization is
  available for download.

Finally, Customization Manager checks each customization for 
*Compatibility* with the current versions of Sage and Extender.
If the Sage and Extender versions meet the minimum requirements, ``Yes`` is
displayed.
If they do not meet the minimum requirements,
the compatiblity issue, either a Sage or Extender version, is displayed
here.

The Action Buttons
------------------

The actions buttons are enabled and disabled based on the highlighted
customization.  There are two that never change:

- :guilabel:`Close`: close Customization Manager without taking any 
  further action.
- :guilabel:`View Log`: open the log files from the two most recent
  actions and display them in the default text editor.

All the others change state based on whether the highlighted customization
is installed, has an upgrade available, and is compatible.

- :guilabel:`Install`: :ref:`Install a Customization`
- :guilabel:`Upgrade`: :ref:`Upgrade a Customization`
- :guilabel:`Uninstall`: :ref:`Uninstall a Customization`
- :guilabel:`Install Modules`: for sites with multiple companies, 
  :ref:`Install Modules`
