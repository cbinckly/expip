Install and Configure Customization Manager
===========================================

Customization Manager is distributed as an Orchid Extender module for Sage 300.
Once installed using the :guilabel:`Extender --> Setup --> Modules` screen and
configured through :guilabel:`Extender --> Setup --> Custom Table Editor`, 
it can be used to install and manage customizations to Sage 300.

If you haven't already, download the most recent version of the Customization
Manager module before getting started.

:download:`Download Customization Manager<../../expip/vi/EXPIP.poplar.vi>`

Installation Video
------------------

This video walks through all the steps documented below in case you
prefer a video.

.. raw:: html

    <video width="640" controls>
      <source src="https://s3.amazonaws.com/dev.expi/content/expip/customization_manager_install.mp4" type="video/mp4">
      Your browser doesn't support the video tag.
    </video>

Install the Module
------------------

Start by installing the Customization Manager module with Extender.

   1. Open the :guilabel:`Extender --> Setup --> Modules` screen.
   2. Click :guilabel:`Import` and select the ``EXPIP.poplar.vi`` file.
   3. Once the module has imported, close the :guilabel:`Modules` screen.

Configure the Module
--------------------

Customization Manager uses API keys to keep customizations private.
Configure your API key:

   1. Open the :guilabel:`Extender --> Setup --> Custom Table Editor` screen.
   2. From the select box, select :guilabel:`Customization Manager` and click 
      :guilabel:`Load`.
   3. Input the API key and click :guilabel:`Add`.
   4. Close the :guilabel:`Custom Table Editor`.
   
.. note::

    Before starting Customization Manager for the first time, restart the
    Sage desktop to register new components.

Start Customization Manager for the First Time
----------------------------------------------

When Customization Manager is started for the first time, it upgrades the 
`pip`_ package.  The default version of ``pip`` that ships with Extender only
supports an out-dated encryption protocol that is no longer used, making
it impossible to install new packages securely.  

Upgrading ``pip`` can take some time.  First, the customization tries to
perform an in-place upgrade without downloading new packages.  

.. figure:: /static/images/pip_upgrade_1.png
    
    Before trying an in-place upgrade, this message is displayed.

If this isn't possible, a new version is downloaded and installed.  

.. figure:: /static/images/pip_upgrade_2.png
    
    If an in-place upgrade isn't possible, a new version is downloaded.

Together these steps can take up to three minutes.  But the upgrade is only
required on first start - you won't see it again!

With the upgrade complete, the Customization Manager screen opens.

.. image:: /static/images/customization_manager.png

Install the Customization Manager Package
-----------------------------------------

Customization Manager can manage itself.  By installing the packaged version
of the module, you'll be able to upgrade it at the click of a button - no more
file downloads.  Installing the Customization Manager package is highly
recommended.

1. Highlight the :guilabel:`expip` package.
2. Click the :guilabel:`Install` button.

All done. When new verions are released, you will see an upgrade available for 
the ``expip`` package.

.. _pip: https://pypi.org/project/pip/
