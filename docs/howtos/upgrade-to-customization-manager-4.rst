.. _customization-manager-4-upgrade:

Upgrade to Customization Manager 4
==================================

Customization Manager 4.0.0 adds a number of new features,
such as postinstall scripts, test data fixture support, and
improved version detection.  It also has an improved
interface that makes managing customizations easier.

If you're running an older version of Customization Manager, 
you probably installed it from a module file.  Once installed
from a module file, Customization Manager can 
:ref:`upgrade itself<Upgrade with Customization Manager>`.
This is the easiest path to upgrade.

If you'd rather install the module file again manually, see the
instructions in the :ref:`Upgrade with Module File` section instead.

Upgrade with Customization Manager
----------------------------------

To get Customization Manager to manager and upgrade itself, 
install the ``expip`` package:

1. Open Customization Manager.
2. Select the ``expip`` package.
3. Click :guilabel:`Install` or :guilabel:`Upgrade`.

That's it. Customization Manager is now fully upgraded and the
Desktop environment fixed up.

Upgrade with Module File
------------------------

When upgrading from a previous version of Customization Manager,
a new icon will be created on the desktop.  For users that are
upgrading, this will result in two identical icons, only one
of which will work.

To avoid this situation, remove the existing icon before performing
the upgrade.  To remove the existing icon:

1. Navigate to :guilabel:`Extender --> Setup --> Scripts`.
2. Highlight the ``EXPIP_poplar.py`` file. Right click and select
   :guilabel:`Add to Desktop`.
3. A window with the desktop tree is displayed. Expand 
   :guilabel:`Extender --> Setup`.
4. Highlight the :guilabel:`Customization Manager` icon. Push the
   Delete (``Del``) key on your keyboard to remove the icon.
5. Do not use the :guilabel:`Save` or :guilabel:`Cancel` buttons in this step!
   Close the window using the close ``X`` in the title bar. 

.. warning::

    If you accidentally close the window using a button, fear not, no
    harm has been done.  However, you'll have to repeat the process
    from the beginning.

6. When prompted, save your changes to the desktop layout.
7. Before closing the :guilabel:`Scripts` panel, highlight the 
   ``EXPIP_poplar.py`` file, and push the Delete (``Del``) key on
   your keyboard to delete it - it will be replaced with a new
   script under a different name.
8. Close all windows and restart Sage.

Once the icon is removed, you're free to upgrade Customization Manager
by installing the new version of the module, or selecting the :guilabel:`expip`
customization in Customization Manager and upgrading it.
