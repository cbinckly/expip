Install Modules
===============

In a multi-Company environment, the module files that enable a customization
are only enabled in the Company from which the customization is first 
installed. The customization package is installed system wide and is 
easily enabled in other companies.

To enable a customization in other companies, you will need to install the 
customization modules:

1. Connect to the Company the customization needs installing in.
2. Open the :guilabel:`Extender --> Setup --> Customization Manager` screen.
3. A grid displays all the customizations that have been built for you. 
   Highlight the customization and click :guilabel:`Install Modules`.
4. A message is displayed with the result.  
5. Close the Customization Manager window.

Done. Restart the Sage Desktop to allow Extender to register the changes
in this company.

The Install Modules Button is disabled
--------------------------------------

The :guilabel:`Install Modules` button will be disabled if:

- the customization is not yet installed, in which case its status will be
  ``Available``;
- the current Sage or Extender versions are not compatible with the 
  customization, the compatibility field will indicate which versions are
  required.

If you're having trouble installing modules for a customization, `contact us`_.

.. _contact us: mailto:chris@poplars.dev
