How-Tos
===============================================================================

This is a collection of guides detailing the steps to carry out
common tasks with Customization Manager.  

.. toctree::
   :maxdepth: 1

   howtos/install_and_configure_customization_manager
   howtos/work_with_customization_manager
   howtos/install_customization
   howtos/upgrade_customization
   howtos/uninstall_customization
   howtos/install_modules
   howtos/upgrade-to-customization-manager-4
   howtos/uninstall_customization_manager
