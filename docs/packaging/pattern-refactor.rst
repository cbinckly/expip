##########################################################################
Pattern for Customizations using Packages
##########################################################################

Customization Manager enables a whole host of
new development and code deployment workflows for Sage 300 customizations.
In particular, it makes code re-use, both within and across projects,
straight-forward.  

This article walks through
refactoring an Extender module into three components:

- Extender module focused only on client business logic (``VOLUMETAX.vi``)
- Package containing general functions (``extools``)
- Package containing module specific functions (``2665093-volumetax``)

The Customer Problem
********************

Let's start with a real use case:
    
    A food distributor requires Sage calculate a tax on some liquid
    products based on volume and not dollar value for sales in 
    a specific area.

    This applies to Orders, Invoices, and Shipments.

The First Solution
******************

After some back and forth with the client, a design emerges:

- All affected items will have a tax class of "VOLUME"
- All affected customers will have a tax class of "VOLUME"
- Item volume can be determined by using the conversion factors in IC.

When a user inputs a new detail line in any of the documents, if the item
and customer both have a tax class of VOLUME, manually calculate the
item tax using the tax rate from the VOLUME tax table multiplied by the item
quantity scaled by the conversion factor.

Because the total tax on a document header must match the sum of the detail 
lines tax will also need to be calculated for the document before it is posted
if it contains items taxed by volume.

First Cut of the Module
***********************

Using Extender we can create view scripts that will perform the tax 
calculations and update the document headers and details when the tax
classes are set for the customer and item.  A script will be required for each
view, so we will wrap them in a module to make them more manageable.

Let's start a new module file.

.. code-block:: ini
    :lineno-start: 1
    :linenos:

    [MODULE]
    id=VOLUMETAX
    name=Volume Tax
    desc=Adjust order, shipping, invoice, and credit note taxes based on volume.
    company=2665093 Ontario Inc.
    version=0.1
    website=None


Now, frame out the first two scripts.  Let's start with invoices.

.. code-block:: ini
    :lineno-start: 9
    :linenos:

    [SCRIPT]
    FILENAME=VOLUMETAX.INVOICEDETAIL
    >>> SCRIPT >>>
    ###############################################################################
    #  OE0400 - Volume Tax - Invoice Details
    #
    #  This script automatically calculates the Volume tax for an invoice detail
    #  line If and only if both the customer and the item have a tax authority of
    #  TAXAUTH (default VOLUME)
    ###############################################################################
    from accpac import *
    <<< SCRIPT <<<

    [VIEWSCRIPT]
    VIEWID=OE0400
    UNIQID=2019050100000004
    ACTIVE=1
    ORDER=0
    SCRIPT=VOLUMETAX.INVOICEDETAIL

    [SCRIPT]
    FILENAME=VOLUMETAX.INVOICEHEADER
    >>> SCRIPT >>>
    ################################################################################
    #  OE0420 - Volume Tax - Invoice Header
    #
    #  This script automatically calculates the Tax Base 1 and Tax Amount 1
    #  for an invoice header if the customer has a tax authority of
    #  TAXAUTH (default VOLUME)
    ################################################################################
    from accpac import *
    <<< SCRIPT <<<

    [VIEWSCRIPT]
    VIEWID=OE0420
    UNIQID=2019050100000004
    ACTIVE=1
    ORDER=0
    SCRIPT=VOLUMETAX.INVOICEDETAIL



Let's just dive in and start writing the code for the invoice detail lines
(OE0400) script.

We will need access to the customer information to find their tax authority,
so it would be useful to have access to the composed OE document header view.

.. code-block:: python
    :linenos:

    from accpac import *

    def onBeforeCompose(event):
        global oehdr
        if len(event.views):
            oehdr = event.views[0]
        return Continue

With that available, try implementing the meat in ``onBeforeInsert``.  A good
place to start is to check the pre-conditions for calculating the tax are met.

.. code-block:: python
    :lineno-start: 10
    :linenos:

    TAXAUTH = "VOLUME"

    def onBeforeInsert():
        global oehdr

        # If the view is not composed, no action to be taken
        if not oehdr:
            showMessageBox("Volume Tax: Order Detail is not composed. Continuing.")
            return Continue

        # If the customer tax authority doesn't match, no action
        if oehdr.get("TAUTH1") != TAXAUTH:
            showMessageBox("Volume Tax: Order Tax Auth is {}, not {}. Continuing".format(
                oehdr.get("TAUTH1"), TAXAUTH))
            return Continue

        # If the item tax authority doesn't match, continue
        if me.get("TAUTH1") != TAXAUTH:
            showMessageBox("Volume Tax: Item Tax Auth is {}, not {}. Continuing".format(
                me.get("TAUTH1"), TAXAUTH))
            return Continue

That is a good start.  But there is a pattern emerging, showing a message box 
with the *Volume Tax:* leading text.  We can refactor that out into a method so
we never forget to include it and the user always knows our script is the one 
speaking.

.. code-block:: python

    TAXAUTH = "VOLUME"
    TITLE = "Volume Tax"

    def alert(message):
        showMessageBox("{}: {}".format(TITLE, message))

    def onBeforeInsert():
        global oehdr

        # If the view is not composed, no action
        if not oehdr:
            alert("Order Detail is not composed. Continuing.")
            return Continue

        # If the customer tax authority doesn't match, no action
        if oehdr.get("TAUTH1") != TAXAUTH:
            alert("Order Tax Auth is {}, not {}. Continuing".format(
                oehdr.get("TAUTH1"), TAXAUTH))
            return Continue

        # If the detail line tax authority doesn't match, no action
        if me.get("TAUTH1") != TAXAUTH:
            alert("Item Tax Auth is {}, not {}. Continuing".format(
                me.get("TAUTH1"), TAXAUTH))
            return Continue

That's better. I am not sure about displaying the label in line, instead
it may be better to add newlines between the title and the message.  Now
I can decide later and will only need to change the ``alert`` function.

Now that we know the pre-conditions are being met, let's do the manual
tax calculation for the detail line.

.. code-block:: python

    def onBeforeInsert():
        # ... pre-conditions checking ...

        # Set tax to calculate manually
        _put = oehdr.put("AUTOTAXCAL", 0)
        if _put != 0:
            alert("Could not disable Auto Tax Calculation.")
            return Continue

        # Checkpoint - debugging to make sure we are capturing the right 
        # details.
        alert("Tax Authority for Item and Customer is {}".format(TAXAUTH))

        # Get tax rate from the tax rate tables.
        tax_rate_view = openView("TX0004", 2)
        _auth = tax_rate_view.put("AUTHORITY", TAXAUTH)
        _sales = tax_rate_view.put("TTYPE", 1)
        _bclass = tax_rate_view.put("BUYERCLASS", oehdr.get("TCLASS1"))

        # Make sure the puts succeded, they return 0 on success.
        if sum([_auth, _sales, _bclass]) != 0:
            alert("Unable to put values to read tax rate.")
            return Continue

        # The read fails because the tax table does not exist.
        if tax_rate_view.read() != 0:
            alert("Could not find tax rate table entry for {}".format(
                    TAXAUTH))
            return Continue

        # Calculate the item tax, using the scaled quantity as the base.
        tax_base = me.get("QTYSHIPPED") * me.get("CONVERSION")
        tax_class = me.get("TCLASS1")
        tax_rate = tax_rate_view.get("ITEMRATE{}".format(tax_class))
        tax_owed = tax_base * (tax_rate / 100)

        # Checkpoint
        alert("Tax info: base {}, class {}, rate {}, owed {}.".format(
                tax_base, tax_class, tax_rate, tax_owed))

        _tbase = me.put("TBASE1", round(tax_base,2))
        _trate = me.put("TRATE1", round(tax_rate,2))
        _tamt =  me.put("TAMOUNT1", round(tax_owed,2))

        if sum([_tbase, _trate, _tamt]) != 0:
            alert("Could not put item tax base, rate, or amount.")

        return Continue

This is not a bad first implementation.  There are a couple things that
are not intuitive.  For example, the use of python's built in ``sum`` to 
check that view operations have succeeded.  We can improve the readability by
wrapping it in a different name.

.. code-block:: python
    
    def success(*args):
        """Were view operations successful?"""
        if sum(args) == 0:
            return True
        return False

Now we can refactor a little and change the code to use the new success method.

.. code-block:: python

    if sum([_tbase, _trate, _tamt]) != 0:
        alert("Could not put item tax base, rate, or amount.")

becomes 

.. code-block:: python

    if not success(_tbase, _trate, _tamt):
        alert("Could not put item tax base, rate, or amount.")

Alright.  On to the header script.  We will need access to the
detail lines to sum the tax.  Get a reference on compose, just as we did for 
the ``oehdr`` in the detail line script.

.. code-block:: python

    def onBeforeCompose(e):
        global oedtl
        if len(views):
            oedtl = e.views[0]

        return Continue

And into the meat, the onBeforeInsert implementation.  

.. code-block:: python

    def onBeforeInsert():
        global oedtl

        # If the view is not composed, no action
        if not oedtl:
            # View is not composed, no action to be taken
            alert("Order header is not composed. Continuing.")
            return Continue

        # If the header tax authority doesn't match, no action
        if me.get("TAUTH1") != TAXAUTH:
            alert("Item Tax Auth is {}, not {}. Continuing".format(
                me.get("TAUTH1"), TAXAUTH))
            return Continue

        # Set tax to calculate manually
        _put = me.put("AUTOTAXCAL", 0)

        if not success(_put):
            alert("Could not disable Auto Tax Calculation.")
            return Continue

        # Seek to the first line of the detail view.
        _rc = oedtl.recordClear()
        _browse = oedtl.browse("LINETYPE = 1", 1)

        if not success(_rc, _browse):
            alert("Could not get item information.")
            return Continue

        # Sum the tax due for the document header.
        tbase1 = 0
        tamt = 0
        while success(oedtl.fetch()):
            item_base = oedtl.get("TBASE1")
            item_amount = oedtl.get("TAMOUNT1")

            tbase1 += item_base
            tamt += item_amount

        # Checkpoint
        alert("Calculated header totals: base {}, amount {}".format(tbase1, tamt))

        _tbase = me.put("TBASE1", tbase1)
        _tamt =  me.put("TAMOUNT1", tamt)

        if not success(_tbase, _tamt):
            alert("Could not put item tax base or amount in header.")

        return Continue

The ``alert`` and ``success`` functions got reused in this script, so they need
to be copy/pasted in.  Now if I want to add newlines to my alerts, more than 
one change will be required.  I'm not keeping very DRY.

The checkpoints in the code are useful during development but it is easy to
forget they're there and ship them by accident.  It would be nice to toggle 
them on and off like debug logging.  Let's add a ``debug(message)`` function
that only alerts when a constant, ``DEBUG``, is set.

.. code-block:: python
    
    DEBUG = True

    def debug(message):
        if DEBUG:
            showMessageBox("{}\n\n{}: {}".format(rotoID, TITLE, message))

Let's replace all non-essential alerts with debugs and see where we are.

.. code-block:: python
    :linenos:

    [MODULE]
    id=VOLUMETAX
    name=Volume Tax
    desc=Adjust order, shipping, invoice, and credit note taxes based on volume.
    company=2665093 Ontario Inc.
    version=0.1
    website=None

    [SCRIPT]
    FILENAME=VOLUMETAX.INVOICEDETAIL
    >>> SCRIPT >>>
    ###############################################################################
    #  OE0400 - Volume Tax - Invoice Details
    #
    #  This script automatically calculates the Volume tax for an invoice detail
    #  line If and only if both the customer and the item have a tax authority of
    #  TAXAUTH (default VOLUME)
    ###############################################################################
    from accpac import *

    DEBUG = True
    TAXAUTH = "VOLUME"
    TITLE = "Volume Tax"

    # Utility functions

    def alert(message):
        """Show a message box with title."""
        showMessageBox("{}: {}".format(TITLE, message))

    def debug(message):
        """Show a message box with roto ID and title if DEBUG is truthy."""
        if DEBUG:
            showMessageBox("{}\n\n{}: {}".format(rotoID, TITLE, message))

    def success(*args):
        """Were view operations successful?"""
        if sum(args) == 0:
            return True
        return False

    # View hooks

    def onBeforeCompose(event):
        global oehdr
        if len(event.views):
            oehdr = event.views[0]
        return Continue

    def onBeforeInsert():
        global oehdr

        # If the view is not composed, no action
        if not oehdr:
            alert("Order Detail is not composed. Continuing.")
            return Continue

        # If the customer tax authority doesn't match, no action
        if oehdr.get("TAUTH1") != TAXAUTH:
            debug("Order Tax Auth is {}, not {}. Continuing".format(
                oehdr.get("TAUTH1"), TAXAUTH))
            return Continue

        # If the detail line tax authority doesn't match, no action
        if me.get("TAUTH1") != TAXAUTH:
            debug("Item Tax Auth is {}, not {}. Continuing".format(
                me.get("TAUTH1"), TAXAUTH))
            return Continue

        # Set tax to calculate manually
        _put = oehdr.put("AUTOTAXCAL", 0)
        if not success(_put):
            alert("Could not disable Auto Tax Calculation.")
            return Continue

        # Checkpoint - debugging to make sure we are capturing the right 
        # details.
        debug("Tax Authority for Item and Customer is {}".format(TAXAUTH))

        # Get tax rate from the tax rate tables.
        tax_rate_view = openView("TX0004", 2)
        _auth = tax_rate_view.put("AUTHORITY", TAXAUTH)
        _sales = tax_rate_view.put("TTYPE", 1)
        _bclass = tax_rate_view.put("BUYERCLASS", oehdr.get("TCLASS1"))

        # Make sure the puts succeded, they return 0 on success.
        if not success(_auth, _sales, _bclass):
            alert("Unable to put values to read tax rate.")
            return Continue

        # The read fails because the tax table entry does not exist.
        if not success(tax_rate_view.read()):
            alert("Could not find tax rate table entry for {}".format(
                    TAXAUTH))
            return Continue

        # Calculate the item tax, using the scaled quantity as the base.
        tax_base = me.get("QTYSHIPPED") * me.get("CONVERSION")
        tax_class = me.get("TCLASS1")
        tax_rate = tax_rate_view.get("ITEMRATE{}".format(tax_class))
        tax_owed = tax_base * (tax_rate / 100)

        debug("Tax info: base {}, class {}, rate {}, owed {}.".format(
                tax_base, tax_class, tax_rate, tax_owed))

        _tbase = me.put("TBASE1", round(tax_base,2))
        _trate = me.put("TRATE1", round(tax_rate,2))
        _tamt =  me.put("TAMOUNT1", round(tax_owed,2))

        if not success(_tbase, _trate, _tamt):
            alert("Could not put item tax base, rate, or amount.")

        return Continue
    <<< SCRIPT <<<

    [VIEWSCRIPT]
    VIEWID=OE0400
    UNIQID=2019050100000004
    ACTIVE=1
    ORDER=0
    SCRIPT=VOLUMETAX.INVOICEDETAIL

    [SCRIPT]
    FILENAME=VOLUMETAX.INVOICEHEADER
    >>> SCRIPT >>>
    ################################################################################
    #  OE0420 - Volume Tax - Invoice Header
    #
    #  This script automatically calculates the Tax Base 1 and Tax Amount 1
    #  for an invoice header if the customer has a tax authority of
    #  TAXAUTH (default VOLUME)
    ################################################################################
    from accpac import *

    DEBUG = True
    TAXAUTH = "VOLUME"
    TITLE = "Volume Tax"

    # Utility functions

    def alert(message):
        """Show a message box with title."""
        showMessageBox("{}: {}".format(TITLE, message))

    def debug(message):
        """Show a message box with roto ID and title if DEBUG is truthy."""
        if DEBUG:
            showMessageBox("{}\n\n{}: {}".format(rotoID, TITLE, message))

    def success(*args):
        """Were view operations successful?"""
        if sum(args) == 0:
            return True
        return False

    # View hooks

    def onBeforeCompose(e):
        global oedtl
        if len(views):
            oedtl = e.views[0]

        return Continue

    def onBeforeInsert():
        global oedtl

        # If the view is not composed, no action
        if not oedtl:
            # View is not composed, no action to be taken
            alert("Order header is not composed. Continuing.")
            return Continue

        # If the header tax authority doesn't match, no action
        if me.get("TAUTH1") != TAXAUTH:
            alert("Item Tax Auth is {}, not {}. Continuing".format(
                me.get("TAUTH1"), TAXAUTH))
            return Continue

        # Set tax to calculate manually
        _put = me.put("AUTOTAXCAL", 0)

        if not success(_put):
            alert("Could not disable Auto Tax Calculation.")
            return Continue

        # Seek to the first line of the detail view.
        _rc = oedtl.recordClear()
        _browse = oedtl.browse("LINETYPE = 1", 1)

        if not success(_rc, _browse):
            alert("Could not get item information.")
            return Continue

        # Sum the tax due for the document header.
        tbase1 = 0
        tamt = 0
        while success(oedtl.fetch()):
            item_base = oedtl.get("TBASE1")
            item_amount = oedtl.get("TAMOUNT1")

            tbase1 += item_base
            tamt += item_amount

        # Checkpoint
        debug("Calculated header totals: base {}, amount {}".format(tbase1, tamt))

        _tbase = me.put("TBASE1", tbase1)
        _tamt =  me.put("TAMOUNT1", tamt)

        if not success(_tbase, _tamt):
            debug("Could not put item tax base or amount in header.")

        return Continue
    <<< SCRIPT <<<

    [VIEWSCRIPT]
    VIEWID=OE0420
    UNIQID=2019050100000004
    ACTIVE=1
    ORDER=0
    SCRIPT=VOLUMETAX.INVOICEHEADER

There is a fair bit of duplication across the scripts, and it isn't going to 
any better. The remaining two document types require nearly idenitcal code. 
The only significant change is the name of the quantity field, ordered or 
shipped.

The full module has six copies of all utility functions and constants.  The
logic to handle the detail lines is there in triplicate, same for the header.

So when the client comes back and says the title definitely needs newlines
in the alerts, there are six changes instead of one.  And it gtes worse...

A Change in Requirements
~~~~~~~~~~~~~~~~~~~~~~~~

After testing the module, the client provides feedback:

- Alerts need newlines
- The conversion isn't correct, can we use an optional field on the item 
  instead?  They have one setup called VOLCONV.

Alright, let's add a some code to all three detail scripts to look up a new
conversion factor from an item optional field.

.. code-block:: python
    
    CONVOPTFIELD = "VOLCONV"
    
    def onBeforeInsert():
        ...
        # The read fails because the tax table entry does not exist.
        if not success(tax_rate_view.read()):
            alert("Could not find tax rate table entry for {}".format(
                    TAXAUTH))
            return Continue

        icof = openView("IC0313")
        if not icof:
            alert("Failed to open the optional field view. "
                    " Volume tax calculations disabled.")
            return Continue

        _rc = icof.recordClear()
        _pi = icof.put("ITEMNO", itemno)
        _po = icof.put("OPTFIELD", CONVOPTFIELD)
        _r = icof.read()
        conv = icof.get("VALUE")

        if not success(_rc, _pi, _po, _r) or not conv:
            _alert("Failed to read the {} optional field for {}."
                   " Volume tax calculations disabled.".format(
                        CONVOPTFIELD, itemno))
            return Continue

        tax_base = me.get("QTYSHIPPED") * conv
        ...

With that change in place, the scripts are working for the client. But we have 
a module with over 1000 lines of code to express two very simple pieces of
business logic.

The Logic of Modules
~~~~~~~~~~~~~~~~~~~~

Let's try something, strip the module down and take all code related to 
interacting with the views out.  The module code should read like a concise
statement of the client's business logic.

Where does the code go?  Reviewing what we've written, what if we were to:

- Put the utility functions in a library that can be used across projects into
  a package.
- Put the constants and code related to client specific code (like the tax
  calculation) into another package.

There are a few other utility functions hiding in there, we could have 
function to:

- Check the pre-requisites
- Retrieving the item tax rate from the tax tables
- Iterating over all order lines
- Retrieving the value of an optional field

With this in place, the module code will be significantly reduced and 
our code will actually be reusable and maintainable.

extools Package
~~~~~~~~~~~~~~~

Let's start with the functions that we could potentially reuse across projects:

- ``alert`` and ``debug`` for notifications
- ``success`` for testing the results of view calls
- ``all_lines_in(detail_view)`` a new python generator helper to wrap opening
  and seeking to the first record and yielding all lines of a detail view.
- ``item_optfield_value(item, field)`` a helper to open the item optional field
  view and return the value of the field or None if unset.

.. code-block:: python
    
    # extools.py
    from accpac import showMessageBox

    def alert(message):
        """Show a message box with title."""
        showMessageBox("{}: {}".format(TITLE, message))

    def debug(message):
        """Show a message box with roto ID and title if DEBUG is truthy."""
        if DEBUG:
            showMessageBox("{}\n\n{}: {}".format(rotoID, TITLE, message))

    def success(*args):
        """Were view operations successful?"""
        if sum(args) == 0:
            return True
        return False

    def all_lines_in(detail_view):
        """Seeks the detail view to the first record and yields each one."""
        _rc = detail_view.recordClear()
        _browse = detail_view.browse("LINETYPE = 1", 1)

        if not success(_rc, _browse):
            alert("Could not get detail view lines.")
            return

        while success(detail_view.fetch()):
            yield detail_view

    def item_optfield_value(itemno, field):
        """Get the value of an item optional field or None if unset."""
        icof = openView("IC0313")
        if not icof:
            alert("Failed to open the optional field view. "
                    " Volume tax calculations disabled.")
            return None

        _rc = icof.recordClear()
        _pi = icof.put("ITEMNO", itemno)
        _po = icof.put("OPTFIELD", field)
        _r = icof.read()
        val = icof.get("VALUE")

        if not success(_rc, _pi, _po, _r) or not val: 
            _alert("Failed to read the {} optional field for {}."
                   " Volume tax calculations disabled.".format(
                        field, itemno))
            return None

        return val

    def itemrate_for_authority(tax_auth, header_tax_class, item_tax_class,
                               tax_type=1):
        """Get the itemrate for a given authority and item/header tax class."""
        try:
            tax_rate_view = openView("TX0004", 99)
            _auth = tax_rate_view.put("AUTHORITY", tax_auth)
            _sales = tax_rate_view.put("TTYPE", tax_type)
            _bclass = tax_rate_view.put("BUYERCLASS", header_tax_class)

            if not success(_auth, _sales, _bclass):
                alert("Unable to put values to read tax rate.")
                return None

            # The read fails if the tax table entry does not exist.
            if not success(tax_rate_view.read()):
                alert("Could not find tax rate table entry for {}".format(
                        taxauth))
                return None

            # Return the correct item rate.
            return tax_rate_view.get("ITEMRATE{}".format(item_tax_class))
        finally:
            # Make sure that, regardless of what happens, the tax rate view is
            # closed if it has been opened.
            if tax_rate_view:
                tax_rate_view.close()

There is a minor problem here: ``alert`` and ``debug`` depend on the ``TITLE``
constant.  For now, parameterize: ``alert(title, message)`` and ``debug(title, 
message)``.

There are also advantages: by focusing only on the function out of 
the context of the overall flow, I realized that I was leaving the tax rate
view open and dangling. In the function it is wrapped in a try/finally so
in all cases the view is closed properly.

Project Package
~~~~~~~~~~~~~~~

The project package contains all the code and constants specific to this 
project.

.. code-block:: python

    # 2665093-volumetax.py

    DEBUG = True
    TITLE = "Volume Tax"
    TAXAUTH = "VOLUME"
    CONVOPTFIELD = "VOLCONV"

    def check_prereqs(header_tax_auth, item_tax_auth=None): 

        # If the customer tax authority doesn't match, no action
        if header_tax_auth != TAXAUTH:
            debug("Order Tax Auth is {}, not {}. Continuing".format(
                header_tax_auth, TAXAUTH))
            return False

        # If the detail line tax authority doesn't match, no action
        if item_tax_auth and item_tax_auth != TAXAUTH:
            debug("Item Tax Auth is {}, not {}. Continuing".format(
                item_tax_auth, TAXAUTH))
            return False

        return True

First Script Refactor
~~~~~~~~~~~~~~~~~~~~~

Alright, with a toolbox full of functions, let's try refactoring the 
invoice detail script.

.. code-block:: python

    ###############################################################################
    #  OE0400 - Volume Tax - Invoice Details
    #
    #  This script automatically calculates the Volume tax for an invoice detail
    #  line If and only if both the customer and the item have a tax authority of
    #  TAXAUTH (default VOLUME)
    ###############################################################################
    from accpac import *
    from extools import (alert, 
                         debug, 
                         success, 
                         all_lines_in, 
                         item_optfield_value,
                         itemrate_for_authority, )
    from 2665093-volumetax import (TITLE, 
                                   DEBUG, 
                                   CONVOPTFIELD, 
                                   TAXAUTH, 
                                   check_prereqs, )

    # View hooks

    def onBeforeCompose(event):
        global oehdr
        if len(event.views):
            oehdr = event.views[0]
        return Continue

    def onBeforeInsert():
        global oehdr

        # If the view is not composed, no action
        if not oehdr:
            alert("Order Detail is not composed. Continuing.")
            return Continue

        # If the customer/item tax authority doesn't match, no action
        if not check_prereqs(oehdr.get("TAUTH1"), me.get("TAUTH1")):
            return Continue

        # Set tax to calculate manually
        _put = oehdr.put("AUTOTAXCAL", 0)
        if not success(_put):
            alert("Could not disable Auto Tax Calculation.")
            return Continue

        # Checkpoint - debugging to make sure we are capturing the right 
        # details.
        debug("Tax Authority for Item and Customer is {}".format(TAXAUTH))

        itemno = me.get("ITEM")
        conversion = item_optfield_value(itemno, CONVOPTFIELD)
        tax_rate = itemrate_for_authority(
            TAXAUTH, oehdr.get("TCLASS1"), me.get("TCLASS1"))

        if not (conversion and tax_rate):
            alert("Unable to get conversion or tax rate. Volume tax "
                  "calculation failed for {}".format(itemno))
            return Continue

        # Calculate the item tax, using the scaled quantity as the base.
        tax_base = me.get("QTYSHIPPED") * conversion
        tax_owed = tax_base * (tax_rate / 100)

        debug("Tax info: base {}, class {}, rate {}, owed {}.".format(
                tax_base, tax_class, tax_rate, tax_owed))

        _tbase = me.put("TBASE1", round(tax_base,2))
        _trate = me.put("TRATE1", round(tax_rate,2))
        _tamt =  me.put("TAMOUNT1", round(tax_owed,2))

        if not success(_tbase, _trate, _tamt):
            alert("Could not put item tax base, rate, or amount.")

        return Continue

The script has been trimmed from 117 to 74 lines and is much more legible.

We can take it farther, although that may not be desirable. Readable and
maintainable trumps concise (particularly convoluted). But let's try to make
a couple more changes:

- Fix the ``TITLE`` issue.
- Move to a more Pythonic exception based error handing.
- Put all view interactions in functions.

Maintaining a Little State
~~~~~~~~~~~~~~~~~~~~~~~~~~

That pesky title.  Changing the API to inlude the title every time 
defeats the purpose of refactoring out the ``alert`` method in the
first place!

We can work around this by retaining a little state.  Let's introduce a
Notify class that stores the title and can be used to toggle debugging.

.. code-block:: python
    
    # extools.py
    from accpac import (showMessageBox, rotoID, openView)

    class ExNotify():
        ALERT = 50
        DEBUG = 20

        def __init__(self, title, level=None):
            self.title = title
            if not level:
                level = self.ALERT
            self.level = level

        def alert(self, message):
            """Show a message box with title."""
            if self.level <= self.ALERT:
                showMessageBox("{}: {}".format(self.title, message))

        def debug(title, message):
            """Show a message box with rotoID and title if DEBUG is enabled."""
            if self.level <= self.DEBUG:
                showMessageBox("{}\n\n{}: {}".format(rotoID, title, message))

Now, we can create an instance of a notifier at the top of our scripts and use
it throughout.

Aside: Exceptions
~~~~~~~~~~~~~~~~~

Choosing a scheme for handling errors in code can be challenging. Although
there are many choices, consistency is key. So let's choose between using
Python's native exceptions or sticking with a return value approach as
implemented in Extender.

The return value approach has been advocated for a lot recently. I think that
the Go language, which uses a pattern of multiple returns ``(result, error)``,
helped to restore weight.  Using return values makes flow of control very easy
to understand.

Python has a rich exception handling system.  The ``try/except/else/finally``
construct is powerful and flexible but makes those gains at the expense of
simple control flow.

Languages like Go that prefer return values often provide another facility, 
such as Go's ``defer``, that can be used to clean up after function execution.
This is key for resource management in the multiple returns approach.  Without
it, every time an error is encountered, the code to clean up must be repeated
before the return.

Python's ``defer`` is ``finally``.  For this reason, I lean towards exceptions
in Python.  The control flow is pretty straight forward once you get used to
it.

Finally, exceptions need to be used consistently.  I favour custom exception
wrappers that encapsulate a message that can be presented to the user.  This
means error messages can be written close to the errors and leaves the option
to override higher op the stack.

Convention:
    - Custom code raises custom exceptions
    - Custom exceptions should be subclassed so the user (developer) can 
      determine granularity of handling
    - Exceptions contain a single phrase message explaining the issue suitable
      for presentation to a user.
    - Exception messages contain a single phrase or sentence that begins with
      a lowercase letter and does not terminal with a period.
    - Custom exceptions triggered by other exceptions contain a reference
      to the original.

Adding Exception Handling
~~~~~~~~~~~~~~~~~~~~~~~~~

As a general best practices, libraries should only ever speak to the user if 
they are configured for debugging. Our libraries are full of calls to ``alert``
that should be moved up into the module. Wherever we find one, there is a good
chance it should be replaced by an exception.

.. code-block:: python
    
    # extools.py
    from accpac import (showMessageBox, rotoID, openView)

    class ExToolsError(Exception):
        def __init__(self, message, original_exception=None)
            self.message = message
            self.original_exception = None

    class ExToolsViewError(ExToolsError): pass

    class ExNotify():
        ALERT = 50
        DEBUG = 20

        def __init__(self, title, level=None):
            self.title = title
            if not level:
                level = self.ALERT
            self.level = level

        def alert(self, message):
            """Show a message box with title."""
            if self.level <= self.ALERT:
                showMessageBox("{}: {}".format(self.title, message))

        def debug(title, message):
            """Show a message box with rotoID and title if DEBUG is enabled."""
            if self.level <= self.DEBUG:
                showMessageBox("{}\n\n{}: {}".format(rotoID, title, message))

        def __str__(self):
            return "ExNotify({}, {})".format(self.title, self.level)

    def success(*args):
        """Were view operations successful?"""
        if sum(args) == 0:
            return True
        return False

    def all_lines_in(detail_view):
        """Seeks the detail view to the first record and yields each one."""
        _rc = detail_view.recordClear()
        _browse = detail_view.browse("LINETYPE = 1", 1)

        if not success(_rc, _browse):
            raise ExToolsViewError("could not seek to first detail line")

        while success(detail_view.fetch()):
            yield detail_view

    def item_optfield_value(itemno, field):
        """Get the value of an item optional field or None if unset."""
        icof = openView("IC0313")
        if not icof:
            raise ExToolsViewError("failed to open the optional field view")

        _rc = icof.recordClear()
        _pi = icof.put("ITEMNO", itemno)
        _po = icof.put("OPTFIELD", field)
        _r = icof.read()
        val = icof.get("VALUE")

        if not success(_rc, _pi, _po, _r) or not val: 
            raise ExToolsViewError(
                "failed to read the {} optional field for {}".format(
                        field, itemno))
            return None

        return val

    def itemrate_for_authority(tax_auth, header_tax_class, item_tax_class,
                               tax_type=1):
        """Get the itemrate for a given authority and item/header tax class."""
        try:
            tax_rate_view = openView("TX0004", 99)
            _auth = tax_rate_view.put("AUTHORITY", tax_auth)
            _sales = tax_rate_view.put("TTYPE", tax_type)
            _bclass = tax_rate_view.put("BUYERCLASS", header_tax_class)

            if not success(_auth, _sales, _bclass):
                raise ExToolsViewError("failed search for tax rate")

            # The read fails if the tax table entry does not exist.
            if not success(tax_rate_view.read()):
                raise ExToolsViewError("no tax rate table entry for {}".format(
                        taxauth))

            # Return the correct item rate.
            return tax_rate_view.get("ITEMRATE{}".format(item_tax_class))
        finally:
            # Make sure that, regardless of what happens, the tax rate view is
            # closed if it has been opened.
            if tax_rate_view:
                tax_rate_view.close()

Now, let's make the required changes in the module:

.. code-block:: python

    ###############################################################################
    #  OE0400 - Volume Tax - Invoice Details
    #
    #  This script automatically calculates the Volume tax for an invoice detail
    #  line If and only if both the customer and the item have a tax authority of
    #  TAXAUTH (default VOLUME)
    ###############################################################################
    import accpac
    from extools import (ExNotify, 
                         success, 
                         all_lines_in, 
                         item_optfield_value,
                         itemrate_for_authority, 
                         ExToolsViewError, )
    from 2665093-volumetax import (TITLE, 
                                   DEBUG, 
                                   CONVOPTFIELD, 
                                   TAXAUTH, 
                                   check_prereqs, )

    # View hooks

    exn = ExNotify(TITLE)

    def onBeforeCompose(event):
        global oehdr
        if len(event.views):
            oehdr = event.views[0]
        return Continue

    def onBeforeInsert():
        global oehdr

        # If the view is not composed, no action
        if not oehdr:
            exn.alert("Order Detail is not composed. Continuing.")
            return Continue

        # If the customer/item tax authority doesn't match, no action
        if not check_prereqs(oehdr.get("TAUTH1"), me.get("TAUTH1")):
            return Continue

        # Set tax to calculate manually
        _put = oehdr.put("AUTOTAXCAL", 0)
        if not success(_put):
            exn.alert("Could not disable Auto Tax Calculation.")
            return Continue

        # Checkpoint - debugging to make sure we are capturing the right 
        # details.
        debug("Tax Authority for Item and Customer is {}".format(TAXAUTH))

        itemno = me.get("ITEM")
        try:
            conversion = item_optfield_value(itemno, CONVOPTFIELD)
            tax_rate = itemrate_for_authority(
                TAXAUTH, oehdr.get("TCLASS1"), me.get("TCLASS1"))
        except ExToolsViewError as e:
            exn.alert("Error getting item and tax information: {}. "
                      "Volume tax calculation failed for {}".format(
                      e.message, itemno))

        # Calculate the item tax, using the scaled quantity as the base.
        tax_base = me.get("QTYSHIPPED") * conversion
        tax_owed = tax_base * (tax_rate / 100)

        exn.debug("Tax info: base {}, class {}, rate {}, owed {}.".format(
                tax_base, tax_class, tax_rate, tax_owed))

        _tbase = me.put("TBASE1", round(tax_base,2))
        _trate = me.put("TRATE1", round(tax_rate,2))
        _tamt =  me.put("TAMOUNT1", round(tax_owed,2))

        if not success(_tbase, _trate, _tamt):
            exn.alert("Could not put item tax base, rate, or amount.")

        return Continue

Extracting View Interactions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Now that we're playing with exceptions, let's try removing all view 
interactions and replacing them with sensible functions.  There are a few 
places that need addressing:

- Setting the AUTOTAXCAL field
- Calculating the tax
- Rounding and setting the tax values in the view

Let's make two new methods: ``setup_order(oehdr)`` to set the manual tax
calculation field; ``calculate_and_set_detail_tax_rate(detail_view, tax_base,
tax_rate, round_to=2)`` to calculate and set the tax rate in the view.


.. code-block:: python

    # 2665093-volumetax.py
    from extools import (ExToolsViewError, success, )

    DEBUG = True
    TITLE = "Volume Tax"
    TAXAUTH = "VOLUME"
    CONVOPTFIELD = "VOLCONV"

    class VolumeTaxError(ExToolsViewError): pass

    def check_prereqs(header_tax_auth, item_tax_auth=None): 

        # If the customer tax authority doesn't match, no action
        if header_tax_auth != TAXAUTH:
            debug("Order Tax Auth is {}, not {}. Continuing".format(
                header_tax_auth, TAXAUTH))
            return False

        # If the detail line tax authority doesn't match, no action
        if item_tax_auth and item_tax_auth != TAXAUTH:
            debug("Item Tax Auth is {}, not {}. Continuing".format(
                item_tax_auth, TAXAUTH))
            return False

        return True
    
    def setup_order(oehdr):
        _put = oehdr.put("AUTOTAXCAL", 0)
        if not success(_put):
            raise VolumeTaxError("failed to disable auto tax calculation")

    def calculate_and_set_detail_tax_rate(
            detail_view, tax_base, tax_rate, round_to=2):
        # Calculate the item tax, using the scaled quantity as the base.
        tax_owed = tax_base * (tax_rate / 100)

        _tbase = detail_view.put("TBASE1", round(tax_base,2))
        _trate = detail_view.put("TRATE1", round(tax_rate,2))
        _tamt =  detail_view.put("TAMOUNT1", round(tax_owed,2))

        if not success(_tbase, _trate, _tamt):
            raise VolumeTaxError(
                "failed to put item tax base, rate, or amount")

    def calculate_and_set_header_tax_rate(
            header_view, detail_view, round_to=2):
        # Sum the tax due for the document header.
        tbase1 = 0
        tamt = 0

        for line in all_lines_in(detail_view):
            item_base = line.get("TBASE1")
            item_amount = line.get("TAMOUNT1")

            tbase1 += item_base
            tamt += item_amount

        _tbase = header_view.put("TBASE1", round(tbase1, 2))
        _tamt =  header_view.put("TAMOUNT1", round(tamt, 2))

        if not success(_tbase, _tamt):
            raise VolumeTaxError(
                "failed to put header tax base or amount")

Now let's roll that in to the module.

.. code-block:: python

    ###############################################################################
    #  OE0400 - Volume Tax - Invoice Details
    #
    #  This script automatically calculates the Volume tax for an invoice detail
    #  line If and only if both the customer and the item have a tax authority of
    #  TAXAUTH (default VOLUME)
    ###############################################################################
    import accpac
    from extools import (ExNotify, 
                         ExToolsViewError,
                         success, 
                         all_lines_in, 
                         item_optfield_value,
                         itemrate_for_authority, )
    from 2665093-volumetax import (TITLE, 
                                   DEBUG, 
                                   CONVOPTFIELD, 
                                   TAXAUTH, 
                                   setup_order,
                                   check_prereqs, 
                                   calculate_and_set_detail_tax_rate, )

    exn = ExNotify(TITLE)

    # View Hooks
    def onBeforeCompose(event):
        global oehdr
        if len(event.views):
            oehdr = event.views[0]
        return Continue

    def onBeforeInsert():
        global oehdr

        # If the view is not composed, no action
        if not oehdr:
            exn.alert("Order Detail is not composed. Continuing.")
            return Continue

        # If the customer/item tax authority doesn't match, no action
        if not check_prereqs(oehdr.get("TAUTH1"), me.get("TAUTH1")):
            return Continue

        itemno = me.get("ITEM")
        header_tax_class = oehdr.get("TCLASS1")
        item_tax_class = me.get("TCLASS1")
        quantity = me.get("QTYSHIPPED")

        try:
            # Set tax to calculate manually
            setup_order(oehdr)

            # Calculate the tax base based on optfield conversion factor
            conversion = item_optfield_value(itemno, CONVOPTFIELD)
            tax_base = quantity * conversion

            # Calculate the item tax, using the scaled quantity as the base.
            tax_rate = itemrate_for_authority(
                            TAXAUTH, header_tax_class, item_tax_class)
            calculate_and_set_detail_tax_rate(me, tax_base, tax_rate)
        except ExToolsViewError as e:
            exn.alert("Error getting item and tax information: {}. "
                      "Volume tax calculation failed for {}.".format(
                      e.message, itemno))

        return Continue

Our module is now totally focused on the customer's business logic, is 
highly readable, and easily extendable. The line count is down to 64 from
117.

What of the header script?

.. code-block:: python

    ################################################################################
    #  OE0420 - Volume Tax - Invoice Header
    #
    #  This script automatically calculates the Tax Base 1 and Tax Amount 1
    #  for an invoice header if the customer has a tax authority of
    #  TAXAUTH (default VOLUME)
    ################################################################################
    from accpac import *
    from extools import (ExNotify, 
                         ExToolsViewError,
                         success, 
                         all_lines_in, )
    from 2665093-volumetax import (TITLE, 
                                   DEBUG, 
                                   CONVOPTFIELD, 
                                   TAXAUTH, 
                                   setup_order,
                                   check_prereqs, 
                                   calculate_and_set_header_tax_rate, )

    exn = ExNotify(TITLE)

    # View hooks
    def onBeforeCompose(e):
        global oedtl
        if len(views):
            oedtl = e.views[0]

        return Continue

    def onBeforeInsert():
        global oedtl

        # If the view is not composed, no action
        if not oedtl:
            return Continue

        # If the header tax authority doesn't match, no action
        if not check_prereqs(me.get("TAUTH1")):
            exn.alert("Item Tax Auth is {}, not {}. Continuing".format(
                    me.get("TAUTH1"), TAXAUTH))
            return Continue

        try:
            # Set tax to calculate manually
            setup_order(me)
            calculate_and_set_header_tax_rate(me, oedtl)
        except ExToolsViewError as e:
            exn.alert("Error getting item and tax information: {}. "
                      "Volume tax calculation failed for {}.".format(
                      e.message, itemno))

        return Continue

Much more concise and to the point.  The module is down to 54 lines from 88.  
Code reuse is solid both within and across scripts.

Where did we start?  The first working implementation had a module header and 
three sets of two (nearly) identical scripts without the optional field
conversion handling. Total line count:

- Module Header: 9
- Header Script: 88 * 3 = 264
- Detail Script: 117 * 3 = 351
- Total = 624

The latest implementation:

- Module Header: 9
- Header Script: 54 * 3 = 162
- Detail Script: 64 * 3 = 192
- Total: 363

The module is now (363/624) =~ 60% of the original - 40% fewer lines to 
maintain, but there were the two modules...

- 2665093-volumetax: 62
- extools: 93
- Total Package lines: 155
- Total Package + Module lines: 363 + 155 = 518

This is where how you count matters; lies, damned lies, and statistics after 
all.  The code in ``extools`` is reusable across other projects as well. It is
code paid forward, saving time in future projects, and improving consistency
across modules.

This becomes even more evident as the solution is scaled up.  Not long after 
delivering a version with the optional field functionality working, a new
requirement emerged: it must work with OE Credit/Debit notes as well.

.. figure:: ../static/images/oe_doctype_count_v_loc.png
    :figwidth: 500px
    :align: center
    
    The number of lines of code using a flat module or supporting packages
    given the number of OE document type scripts required.

Note that for the first case, one OE document type, fewer lines are required 
using the flat module approach.  This makes sense: there is more overhead in 
writing the additional packages and that overhead must be reclaimed through
reuse.

Another perspective we could take is: what is ``extools`` were already 
developed as they will not be before the next project.  In that case,
the package approach always results in less code than a flat module.

.. figure:: ../static/images/oe_doctype_count_v_loc_noex.png
    :figwidth: 500px
    :align: center

    The number of lines of code using a flat module or supporting packages
    given the number of OE document type scripts required.

A Two Script Solution
~~~~~~~~~~~~~~~~~~~~~

Using the package approach, the scripts are very lean and focused on the 
customer problem.  Now that they are, it is clear to me that the six script
idea is unecessary.  I am often inclined to thing that, because of the 
inconsistencies in the Sage view field naming, everything must be custom.
But much is easily parameterized.

The business logic is nearly identical in all cases. In fact, header scripts
are all exactly the same.  There is only a one line difference in the detail
line scripts: the calculation of the tax base uses a different quantity field:

- Orders: QTYORDERED
- Invoices: QTYSHIPPED
- Shipments: QTYSHIPPED
- Credit Notes: QTYRETURN

View scripts accept parameters.  If we take the quantity field name as a script
parameter, we can re-use the same script across all OE document types.

First, we need to be able to reliably check whether a parameter has been 
provided.  If not, the script must raise an error. That sounds like something 
that may happen in other cases as well, so let's add a simple method to 
``extools`` that behaves like the Python ``dict.get(key, default-None)`` helper
and will either return the first accpac paramter (``Parameter1``) or ``None``
if it is unset.

.. code-block:: python
    
    # extools.py
    from accpac import (showMessageBox, rotoID, openView, Parameter1)
    ...

    def get_param1(default=None):
        if not Parameter1:
            return default
        return Parameter1

A quick addition to the pre-reqs will notify the user if the 
parameter is not set.

.. code-block:: python
    
    # 2665093-volumetax
    from extools import (ExNotify, 
                         ExToolsViewError,
                         success, 
                         get_param1,
                         all_lines_in, 
                         item_optfield_value,
                         itemrate_for_authority, )

    ...

    def check_prereqs(header_tax_auth, item_tax_auth=None): 

        # If the customer tax authority doesn't match, no action
        if header_tax_auth != TAXAUTH:
            debug("Order Tax Auth is {}, not {}. Continuing".format(
                header_tax_auth, TAXAUTH))
            return False

        # If the detail line tax authority doesn't match, or the 
        # quantity field is unset, no action
        # 
        if item_tax_auth and (item_tax_auth != TAXAUTH or not get_param1()):
            debug("Item Tax Auth is {} and quantity {}. Continuing".format(
                item_tax_auth, get_param1()))
            return False

        return True

And a small change to the detail line script:

.. code-block:: python

    def onBeforeInsert():
        ...
        itemno = me.get("ITEM")
        header_tax_class = oehdr.get("TCLASS1")
        item_tax_class = me.get("TCLASS1")
        quantity = me.get(get_param1())

That is all. Now both scripts are parameterized and reusable across all 
OE document types.

Total Module Line Count: 290
