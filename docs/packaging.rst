Packaging Customizations
===============================================================================

.. toctree::
   :hidden:
   :maxdepth: 2

   packaging/building-packaged-customizations

Customization Manager enables us to distribute Sage 300 customizations as 
Python packages.  This has a number of advantages, including distribution
leveraging standard Python tools, the ability to automatically install and
manage dependencies, and keeping everything, including the documentation, code,
and test data, together.

Distributing Sage 300 customizations as Python Packages is a new approach.
Although the principles and tools will be familiar to Python developers,
they may be new to others.  Learn more about 
:ref:`Building Packaged Customizations`
