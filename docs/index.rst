.. Customization Manager documentation master file, created by
   sphinx-quickstart on Tue Feb 11 21:46:39 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Customization Manager for Orchid Extender
#########################################

Customization Manager for Orchid Extender unlocks the full potential
of Python for customizing Sage 300.  It enables quick and easy management
of the embedded Python installation used by Extender and supports installation
of most of the 200,000+ packages from the `Python Packaging Index`_ or
any custom package from a version control system (VCS) repository.

:download:`Download Customization Manager <../expip/vi/EXPIP.poplar.vi>`

.. note::
    
    Customization Manager version 9.0.0 has been released with 
    full support for Extender 9 and Python 3.8.8. After upgrading 
    to Extender 9 and activating the Python 3.8.8 environment you
    will need to reinstall all customizations managed by Customization
    Manager.

Many of the customizations currently distributed with Customization
Manager are listed in the `Customization Catalogue`_.

.. image:: static/images/customization_manager.png
    :alt: Customization Manager screen displaying available customizations.

.. toctree::
   :hidden:
   :maxdepth: 3
   :caption: Contents:

   howtos
   packaging

.. contents::
   :depth: 2

What is the problem?
********************

Extender uses an embedded Python installation that does not include the usual
executables or run in a standards compliant shell. This makes it difficult to 
leverage Python's package ecosystem when using Extender to customize Sage 300.

How does it work?
*****************

Customization Manager for Orchid Extender makes it easy to install, 
uninstall, and retrieve the information for customizations.  It works by 
temporarily creating a fully functioning Python environment and using pip,
the standard Python package manager, to manage the Extender Python 
installation.

How do I get a copy?
********************

:download:`Download Customization Manager<../expip/vi/EXPIP.poplar.vi>`.

API keys are used to keep your customizations private. `Contact us`_ if you 
need a key.


How do I...
***************************

Have a look at out :ref:`How-Tos` for quick guides to working with the 
interface and getting common tasks done.

What can I use it for?
**********************

Installing and removing customizations! This alone unlocks lots of new
opportunities for Extender.

Install Sage 300 Customizations 
-------------------------------

If you have a deployment key, Customization Manager allows you to install
customizations that have been created for you.

With a standard key, you can install any of the included customizations.

To see a list of the customizations that are currently being distrubuted
with Customization Manager, have a look at the `Customization Catalogue`_.

With a developer key, you can manage the full underlying Python installation,
publish your own customizations on the index, install customizations from
VCS, perform automated testing of customizations and much more.

Leverage libraries
------------------

Need to `parse an excel file`_, do `calculations with Julian Dates`_, 
`access a database`_ or `web service like AWS`_? Maybe `a little AI`_ to 
perform dynamic validations? 

.. _parse an excel file: https://pypi.org/project/openpyxl/
.. _calculations with Julian Dates: https://pypi.org/project/jdcal/
.. _access a database: https://pypi.org/project/pymssql/
.. _web service like AWS: https://pypi.org/project/boto3/
.. _a little AI: https://pypi.org/project/tensorflow/

Simply build a customization for Customization Manager, leverage the libraries,
and save yourself from recreating the wheel.

Create your own
---------------

Create and publish your own libraries to keep `DRY`_ and maximize code re-use.
Customization code that you use frequently across scripts and re-use the code
from your pajckage to save time, improve maintainability, and deliver highly 
readable code focused on your customer's business logic.

.. _DRY: https://en.wikipedia.org/wiki/Don%27t_repeat_yourself

This approach makes backporting bug-fixes to existing customers simple. Simply
fix the code, publish a new version of the customization, and upgrade the
Customization in one click using the Customization Manager!  No need to
redistribute new module files.

VCS Support
***********

Pip can install directly from a Version Control System.  Supported systems
include git, subversion, mercurial, and bazaar.  This makes development and
testing of customizations much easier: install the customization directly from
the code repository in editable mode and upgrades will use the latest commit.

Customization Manager depends on pip, and can do all the things that pip
can, so check out pip's `docs on working with VCS`_ for supported schemes and
syntax.

Note that to download customizations using a VCS the binaries or supporting
libraries for the VCS must be installed seperately.  At present, the SSH
transport protocol is not supported.

Debugging and Viewing the Log
*****************************

Some customizations cannot be installed. In particular, those that require
custom extensions be compiled in place are not supported. 

To figure out why a particular action failed, or just to see a transcript of
the customization actions performed, use the 'View Log' button.  This will open
the logs for the last two operations in the default viewer for text (.txt) 
files.

Help!
*****

If ever things aren't working as expected, a customization you need won't
install, or you accidentally uninstall pip (please, never do this), drop us a
line and we will help to get you running.

..
    References

.. _pip: https://pypi.org/project/pip/
.. _Python Packaging Index: https://pypi.org
.. _Register for a key: https://expi.dev/account/register/
.. _seven day trial: https://expi.dev
.. _docs on working with VCS: 
    https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support
.. _Customization Catalogue: https://poplars.dev/catalogue.html
.. _Contact us: mailto:chris@poplars.dev
