from setuptools import setup, find_packages

with open("README.rst", "r") as f:
    readme = f.read()

setup(
    name='expip',
    version='9.2.0',
    author='Poplar Development',
    url='https://polars.dev',
    author_email='chris@poplars.dev',
    packages=['expip'],
    package_data={
        '': ['vi/*.vi', 'expi.json', ],
        },
    description='Customization Manager for Sage 300.',
    long_description=readme,
    include_package_data=True,
    install_requires=[
    ],
    # Valid classifiers: https://pypi.org/classifiers/
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Win32 (MS Windows)",
        "Intended Audience :: End Users/Desktop",
        "License :: Other/Proprietary License",
        "Operating System :: Microsoft :: Windows",
        "Programming Language :: Python :: 3.4",
    ],
    keywords=[
        "Orchid", "Extender", "Sage 300", "Automation",
        "Package", "Customization", "Accounting", "Poplar",
    ],
    download_url="https://expi.dev/expip",
)
